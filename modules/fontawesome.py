import streamlit as st


css = '''
I'm importing the font-awesome icons as a stylesheet!
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

<i class="fa-solid fa-download"></i>
<i class="fa-solid fa-square"></i>
<i class="fa-solid fa-dragon"></i>
'''

st.write(css, unsafe_allow_html=True)
