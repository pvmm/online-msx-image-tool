import modules.fontawesome

import streamlit as st
from PIL import Image, ImageDraw

#import numpy as np
#import cv2

from pixelated_image_editor import pixelated_image_editor


#
# Build the screen
#

st.sidebar.header('MSX Image Tool')


#
# Sidebar options
#

# Create a combo box with some options
screen_box = st.sidebar.selectbox('Select screen mode', ['SCREEN2', 'SCREEN4', 'SCREEN5'], index=0)

# Create a toggle button for colour clash that defaults to ON
cc_toggle_button = st.sidebar.checkbox('Activate colour clash', value=True)

# Display the toggle button state
if cc_toggle_button:
    #st.write('Colour clash is ON.')
    pass

# Create a toggle button for sprites that defaults to OFF
sprites_toggle_button = st.sidebar.checkbox('Visible sprites', value=False)

# Display the toggle button state
if sprites_toggle_button:
    #st.write('Sprites are ON.')
    pass

# pixel editor
pe = pixelated_image_editor()
st.write(pe)

# Tabs
tab1, tab2 = st.tabs(["Screen", "Sprite Table"])

# Put image before file_uploader
#placeholder = st.empty()
col1, col2 = st.columns([99, 1])

# Add a slider to zoom in on the image
#zoom = st.slider('Zoom', 1, 10, 1)

# Allow user to upload image
uploaded_file = st.file_uploader('Choose an image', type=['png'])


if uploaded_file is not None:
    # Open image
    image = Image.open(uploaded_file)
    draw = ImageDraw.Draw(image)

    # Resize image
    #zoomed_in = image.resize((int(image.width * zoom), int(image.height * zoom)))
    #resized_image = image.resize((400, 400))

    # Display image
    canvas = col1.image(image)
else:
    # Create an empty image
    image = Image.new('RGB', (400, 400), color='white')
    #zoomed_in = image.resize((int(image.width * zoom), int(image.height * zoom)))
    draw = ImageDraw.Draw(image)

    # Display image
    canvas = col1.image(image)

# Add a button to clear the image
if col2.button('Clear'):
    new_image = Image.new('RGB', (400, 400), color='white')
    draw = ImageDraw.Draw(new_image)

# Get current mouse position
mouse_position = None #st.cursor_position

# If the mouse is pressed, draw a line on the image
if col2.button('Draw'):
    if mouse_position is not None:
        # Get the previous mouse position
        last_position = st.get_state('last_position')
        if last_position is None:
            last_position = mouse_position

        # Draw a line from the previous position to the current position
        draw.line([last_position, mouse_position], fill='black', width=line_thickness)

        # Save the current mouse position
        st.set_state('last_position', mouse_position)

