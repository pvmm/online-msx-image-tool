# online-msx-image-tool

Edit images on the browser with Streamlit and prepare them to be used in a MSX game project.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
All contributions are welcome.

## Authors and acknowledgment
This project was mostly written by Pedro de Medeiros during the development of his own game projects and experiments.

## License
THis project uses the MIT License

## Project status
Active and in development.
