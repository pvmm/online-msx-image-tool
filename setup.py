from setuptools import setup

setup(
    name='msx-image-tool',
    version='1.0',
    py_modules=['msx_image_tool'],
    install_requires=[
        'streamlit',
        'streamlit-drawable-canvas'
        'Pillow',
    ],
    entry_points={
        'console_scripts': [
            'image-drawing-tool = image_drawing_tool:main'
        ]
    },
)

